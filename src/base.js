import Rebase from "re-base";
import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAbofNGYFa2pyonxaSa0E9djGaTV_gAtG4",
  authDomain: "catchoftheday-82473.firebaseapp.com",
  databaseURL: "https://catchoftheday-82473.firebaseio.com"
});

const base = Rebase.createClass(firebase.database());

export { firebaseApp };

export default base;
