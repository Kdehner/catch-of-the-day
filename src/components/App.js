import React from "react";
import PropTypes from "prop-types";
import Header from "./Header";
import Order from "./Order";
import Inventory from "./Inventory";
import sampleFishes from "../sample-fishes";
import Fish from "./Fish";
import base from "../base";

class App extends React.Component {
  state = {
    fishes: {},
    order: {}
  };
  static propTypes = {
    match: PropTypes.object
  };
  componentDidMount() {
    const { params } = this.props.match;
    const localStorageRef = localStorage.getItem(params.storeId);
    if (localStorageRef) {
      this.setState({ order: JSON.parse(localStorageRef) });
    }
    this.ref = base.syncState(`${params.storeId}/fishes`, {
      context: this,
      state: "fishes"
    });
  }
  componentDidUpdate() {
    localStorage.setItem(
      this.props.match.params.storeId,
      JSON.stringify(this.state.order)
    );
  }
  componentWillUnmount() {
    base.removeBinding(this.ref);
  }
  addFish = fish => {
    // Get copy of state object
    const fishes = { ...this.state.fishes };
    // Add a new fish
    fishes[`fish${Date.now()}`] = fish;
    // Set new fishes object to state
    this.setState({ fishes });
  };
  updateFish = (key, updatedFish) => {
    // Take copy of state
    const fishes = { ...this.state.fishes };
    // Update state
    fishes[key] = updatedFish;
    // set state
    this.setState({ fishes });
  };
  deleteFish = key => {
    // Get copy of state
    const fishes = { ...this.state.fishes };
    // update state
    fishes[key] = null;
    // set state
    this.setState({ fishes });
  };
  loadSampleFishes = () => {
    this.setState({ fishes: sampleFishes });
  };
  addToOrder = key => {
    // Take a copy of state
    const order = { ...this.state.order };
    // Either add or updated the number in order
    order[key] = order[key] + 1 || 1;
    // Update state
    this.setState({ order });
  };
  removeFromOrder = key => {
    // Take a copy of state
    const order = { ...this.state.order };
    // Remove item from order
    delete order[key];
    // Update state
    this.setState({ order });
  };
  render() {
    return (
      <div className="catch-of-the-day">
        <div className="menu">
          <Header tagline="Fresh Seafood Market" />
          <ul className="fishes">
            {Object.keys(this.state.fishes).map(key => (
              <Fish
                key={key}
                index={key}
                details={this.state.fishes[key]}
                addToOrder={this.addToOrder}
              />
            ))}
          </ul>
        </div>
        <Order
          fishes={this.state.fishes}
          order={this.state.order}
          removeFromOrder={this.removeFromOrder}
        />
        <Inventory
          addFish={this.addFish}
          updateFish={this.updateFish}
          deleteFish={this.deleteFish}
          loadSampleFishes={this.loadSampleFishes}
          fishes={this.state.fishes}
          storeId={this.props.match.params.storeId}
        />
      </div>
    );
  }
}

export default App;
